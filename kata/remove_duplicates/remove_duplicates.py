"""RemoveDuplicates module"""
from collections import OrderedDict


class RemoveDuplicates:
    """RemoveDuplicates class"""

    @staticmethod
    def remove_duplicate(input_list):
        if not isinstance(input_list, list):
            raise ValueError("IllegalArgumentException", input_list)
        else:
            if len(input_list) == 0:  # more intuitive
                ret_val = []
            else:
                ret_val = list(OrderedDict.fromkeys(input_list))
            return ret_val
