# Feature: RemoveDuplicates

## Narrative:

**As a** customer
**I want** to remove duplicate elements from the input list
**So that** I could have list containing only unique elements

## Scenario_1: Empty list as an input

**Given**: Empty list
**When**: Empty list used as an input
**Then**: Function should return empty list

## Scenario_2: List without duplicates as an input

**Given**: List without duplicates
**When**: List without duplicates used as an input
**Then**: Function should return the same list

## Scenario_3: List with duplicates as an input

**Given**: List with duplicates
**When**: List with duplicates used as an input
**Then**: Function should return the list without duplicates

## Scenario_4: Invalid value as an input

**Given**: Non-list value
**When**: Non list value used as an input
**Then**: Function should raise a ValueError exception