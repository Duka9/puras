"""Test module for RemoveDuplicates class"""

import unittest

from remove_duplicates import RemoveDuplicates


class TestRemoveDuplicates(unittest.TestCase):
    """Test class for RemoveDuplicates class"""

    def test_should_return_equal_when_input_is_empty_list_and_expected_is_the_same(self):
        """Testing Scenario_1, Empty list as an input"""
        input_list = []
        self.assertEqual(RemoveDuplicates.remove_duplicate(input_list), [])

    def test_should_return_equal_when_input_is_list_without_duplicates_and_expected_is_the_same(self):
        """Testing Scenario_2, List without duplicates as an input"""
        input_list = [1, 2, 3]
        self.assertEqual(RemoveDuplicates.remove_duplicate(input_list), [1, 2, 3])

    def test_should_return_not_equal_when_input_is_list_without_duplicates_and_expected_is_not_the_same(self):
        """Testing Scenario_2, List without duplicates as an input"""
        input_list = [1, 2, 3]
        self.assertNotEqual(RemoveDuplicates.remove_duplicate(input_list), [1, 2, 3, 4])

    def test_should_return_equal_when_input_is_list_with_duplicates_and_expected_is_the_list_without_duplicates(self):
        """Testing Scenario_3, List with duplicates as an input"""
        input_list = [1, 1, 2, 2, 3, 3]
        self.assertEqual(RemoveDuplicates.remove_duplicate(input_list), [1, 2, 3])

    def test_should_return_not_equal_when_input_is_list_with_duplicates_and_expected_is_the_list_with_duplicates(self):
        """Testing Scenario_3, List with duplicates as an input"""
        input_list = [1, 1, 2, 2, 3, 3]
        self.assertNotEqual(RemoveDuplicates.remove_duplicate(input_list), [1, 1, 2, 2, 3, 3])

    def test_should_raise_exception_when_list_is_not_an_input(self):
        """Testing Scenario_4, Invalid argument as an input"""
        input_list = 1
        self.assertRaises(ValueError, RemoveDuplicates.remove_duplicate, input_list)


unittest.main()
